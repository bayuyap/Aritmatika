package id.sch.smktelkom_mlg.aritmetika;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class AppActivity extends AppCompatActivity {
    EditText editRupiah;
    RadioButton radioDolar, radioEuro;
    Button btnOkKonversi;
    TextView txtHasil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        editRupiah = (EditText)findViewById(R.id.rp);
        radioDolar = (RadioButton)findViewById(R.id.dallar);
        radioEuro = (RadioButton) findViewById(R.id.euro);
        btnOkKonversi = (Button)findViewById(R.id.oke);
        txtHasil = (TextView)findViewById(R.id.hasil);
        btnOkKonversi.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String nilairupiah =
                        editRupiah.getText().toString();
                if (radioDolar.isChecked()){
                    double hasil =
                            Double.parseDouble(nilairupiah)/13331;
                    String result = Double.toString(hasil);

                    txtHasil.setText(result);

                }

                if (radioEuro.isChecked()){
                    double hasil =
                            Double.parseDouble(nilairupiah)/15000;
                    String result = Double.toString(hasil);

                    txtHasil.setText(result);

                }
            }
        });
    }
}
